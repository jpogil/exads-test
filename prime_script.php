<?php

    for ($i = 1; $i <= 100; $i++) {
        $number = $i;
        
        $multiples = getMultiples($i);
        
        echo $number."\n";
        
        if (count($multiples) < 1) {
            echo "[PRIME]\n";
        } else {
            echo "[".implode(',', $multiples)."]\n";
        }
    }

    function getMultiples($i)
    {
        $multiples = [];

    	for ($d = 2; $d <= $i/2; $d++) {
            if ($i % $d == 0) {
                $multiples[] = $d;
            }
        }

        return $multiples;
    }
<?php 

    namespace abNamespace;
    use Exads\ABTestData;

    class abClass
    {
        public function getData(int $promoId): array
        {
            $abTest = new ABTestData($promoId);
            $promotion = $abTest->getPromotionName();
            $designs = $abTest->getAllDesigns();
            
            return $designs;
        }

        public function getDesign(int $promoId)
        {
            $designToRedirect = null;

            $designs = $this->getData($promoId);
            foreach ($designs as $design) {
                if (empty($designToRedirect) || $designToRedirect['splitPercent'] < $design['splitPercent']) {
                    $designToRedirect = $design;
                }
            }

            return $designToRedirect;
        }
    }
<?php
	$array = [];
    for ($i = 44; $i <= 124; $i++) {
        $array[$i] = chr($i);
    }
    
    $random = rand(44, 124);
    
    unset($array[$random]);
    
    $missing = getMissing($array);
    
    echo $missing."\n";
    
    function getMissing($array)
    {
        for ($i = 44; $i <= 124; $i++) {
            if (empty($array[$i])) {
                return chr($i);
            }
        }
    }
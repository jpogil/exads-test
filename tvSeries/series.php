<?php 
    
    require_once __DIR__ . '/DB.php';

    class Series
    {
        function getFromDB($title = null)
        {
            $DB = new DB();

            $query = "
                SELECT title, channel, gender, week_day, show_time
                FROM tv_series
                JOIN tv_series_intervals on
                    tv_series.id = tv_series_intervales.id_tv_series
            ";

            if ($title) {
                $query .= " 
                    WHERE title = '".$title."'
                ";
            }

            return $DB->runQuery($query);
        }

        function getNextSeries($date = null, $title = null)
        {
            $nextSeries = [];
            $series = $this->getFromDB($title);
            $date = $date ?? now();

            if ($series) {
                foreach ($series as $serie) {
                    $show_day_of_week = date('N', strtotime($serie['week_day']));
                    $today_day_of_week = date('N', strotime($date));


                    $hour_from_date = date('H:i', strtotime($date));

                    if ($today_day_of_week <= $show_day_of_week && strtotime($hour_from_date) <= strtotime($serie['show_time'])) {
                        $nextSeries[] = $serie;
                    }
                }
            }

            return $nextSeries;
        }
    }
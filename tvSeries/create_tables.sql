CREATE DATABASE testDB;

CREATE TABLE testDB.tv_series (
    id int AUTO_INCREMENT,
    title varchar(255) NOT NULL,
    channel varchar(255),
    gender varchar(255),
    PRIMARY KEY (id)
);


CREATE TABLE testDB.tv_series_intervals (
    id_tv_series int,
    week_day varchar(255),
    show_time varchar(255),
    CONSTRAINT FK_ID_TV_SERIES FOREIGN KEY (id_tv_series) REFERENCES tv_series(id)
);


INSERT INTO testDB.tv_series (title, channel, gender)
VALUES 
    ('a', 'channel1', 'male'),
    ('b', 'channel2', 'female'),
    ('c', 'channel3', 'female'); 


INSERT INTO testDB.tv_series_intervals (id_tv_series, week_day, show_time)
VALUES 
    (1, 'monday', '18:00'),
    (1, 'wednesday', '18:00'),
    (2, 'tuesday', '18:00'),
    (2, 'thursday', '18:00'),
    (3, 'wednesday', '19:00'),
    (3, 'friday', '18:00');
<?php 

    class DB 
    {
        private $conn;

        function __construct()
        {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "testDB";

            $this->conn = new mysqli($servername, $username, $password, $dbname);
        }

        function runQuery($query)
        {
            $result = $conn->query($query);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $result[] = $row;
                }
            } else {
                $result = null;
            }

            $this->conn->close();

            return $result;
        }
    }